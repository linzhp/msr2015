OK, here are some thoughts on the paper:

* I think it would be to mention the fine-grain code change distributions are power law in the introduction of the paper. I don't see this as being a research question -- in fact, having this as a research question really detracts from the main focus of the paper, which are the simulation results. The current section where we discuss the fine-grain code changes will still be in the paper, but it will just provide background information on the particular power law distributions we'll be examining.

* The introduction needs to provide some justification for why we're examining fine grain code changes. That is, why choose this particular power law distribution, instead of some other one? It might make sense to bring in the related work section paragraphs as part of a general discussion about the question of power laws in software evolution.

* I feel the introduction should provide an intuitive understanding of preferential attachment and SOC. These are not likely to be well known by your audience, and it's important to build this up. I think the text from III. A. 1 and 2 should go into the introduction.

* The introduction also needs to provide justification for the simulation approach. This is a pretty radical way of making a point within software engineering, and it's not reasonable to just say, oh, and we're doing a simulation, as if that's some completely normal and typical thing that anyone would do.
c.f. Cook2005
* Table II -- spell out Power Law in table headings, don't use "pl". Also, instead of results that are 10^^-9 or 10^^-7, just report " << 0.0001" 

* Section III.B (Simulation Model) needs to provide more background. What, exactly, is being simulated? You need to explicitly state that you are trying to simulate the evolution of a software project by creating a series of simulated changes (simulated commits) to the software. The you need to say what the changes are comprised of -- adding methods, adding calls, adding statements, and deleting methods. Then you can get into the fitness discussion.

* The fitness in III.B is not very clear. The paper states it reflects, "how well the method fits into the context." However, you do not state what "context" means in this situation. Is this organizational context? structural context? evolutionary context? task context? There are lots of different meanings of context.

* What is the initial state of a simulation? Is it a clean slate, or do you build on an existing project?

* I'm very concerned that the paper only describes a single simulation run. I think reviewers will have a very hard time with external validity of a project based on just a single simulation run. I feel it is important to have several simulation runs, and then report on aggregated results from across multiple runs (perhaps showing the results of one run in more detail, to make it more clear exactly what is being done).

* The discussion in IV.A should be part of the simulation discussion itself, not part of the results. Also, because fitness was never defined, this section is hard to understand, since fitness itself is not well defined.

* I think that simulation of commits comes across as pretty arbitrary -- it feels like there needs to be a little more justification on the choice of the parameter value here.

* In IV.B, you simply assert that the distribution is power law, yet earlier in the paper you go through enormous effort and contortions to definitively show that certain distributions follow a power law. The difference in formality is pretty noticeable.
Run the same procedure again, somehow make it work
* In V.A there seems to be a confusions about cause and effect. Is it the case that you directly simulated a self organized criticality process? Or is it the case that you simulated something else, but SOC type behavior emerged from this? It feels to me like it's the latter, but the paper discussion seems to indicate the former.


TODO:
* Turnu 2011 did not simulate delete
* Change type frequency to decide what change to simulate
